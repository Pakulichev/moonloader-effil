package = "moonloader-effil"
version = "1.0.0-1"
source = {
   url = "git+https://gitlab.com/Pakulichev/moonloader-effil.git",
   tag = "v.1.0.0"
}
description = {
   summary = "effil4moonloader",
   homepage = "https://gitlab.com/Pakulichev/moonloader-effil",
   license = "MIT"
}
dependencies = {
   "lua >= 5.1, < 5.4"
}
build = {
   type = "builtin",
   modules = {}
}
